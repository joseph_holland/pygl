from core.base import Base

class Test(Base):
    
    def initialise(self):
        print("Initialising program...")
    
    def update(self):
        self

Test().run()